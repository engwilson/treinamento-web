
(function() {
    "use strict";
    $(function($) {

     
        var vH = $(window).height();
        var vW = $(window).width();       
        $('.fullwidth').css('width', vW);
        $('.fullheight').css('height', vH);
        $('.halfwidth').css('width', vW / 2);
        $('.halfheight').css('height', vH / 2);
        $('.quarterheight').css('height', vH);



//FILTRO
        (function( $ ){
           $.fn.filterPanelTrigger = function() {
                if($(".works-filter-panel").hasClass('slide-to-right'))
                {
                    $('.works-filter-panel').removeClass('slide-to-right');
                }
                else{
                    $('.works-filter-panel').addClass('slide-to-right');
                }
           }; 
        })( jQuery );
        $('.filter-notification a').on('click', function(){
            $().filterPanelTrigger();
        });
        $('.works-filter-panel').on('mouseleave', function(){
            $().filterPanelTrigger();
        });
        $('.works-filter li a').on('click', function(){
            $('.works-filter li a').removeClass('filter-active');
            $(this).addClass('filter-active');
            $('html, body').animate({
                scrollTop: $("#works-container").offset().top-100
            }, 1000);
        });


        if ( $( ".works-container" ).length ) {
            $('.filter-notification a').show();
        }
        else{
            $('.filter-notification a').hide();
        }
        


        
//ISOTOPE
        
        //ISOTOPE GLOBALS
        var $container1 = $('.works-container');


        //ISOTOPE INIT
        $(window).load(function() {

           //checking if all images are loaded
            $container1.imagesLoaded( function() {

                //init isotope once all images are loaded
                $container1.isotope({
                    // options
                    itemSelector: '.works-item',
                    layoutMode: 'masonry',
                    transitionDuration: '0.8s'
                });


                //forcing a perfect masonry layout after initial load
                setTimeout(function() {
                $container1.isotope('layout');
                }, 100);


                // triggering filtering
                $('.works-filter li a').on('click', function() {
                    $('.works-filter li a').removeClass('active');
                    $(this).addClass('active');

                    var selector = $(this).attr('data-filter');
                    $('.works-container').isotope({
                        filter: selector
                    });
                    setTimeout(function() {
                        $container1.isotope('layout');
                    }, 700);
                    return false;
                });


                //Isotope ReLayout on Window Resize event.
                $(window).on('resize', function() {
                    $container1.isotope('layout');
                });

                //Isotope ReLayout on device orientation changes
                window.addEventListener("orientationchange", function() {
                    $container1.isotope('layout');
                }, false);
            });
        });
    });
})();
